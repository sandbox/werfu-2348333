<?php
/**
 * Implements hook_form().
 */
function watchman_admin_form($form, &$form_state) {
	
	$loggers = watchman_get_loggers();
	$options = array();
	foreach ($loggers as $key => $value) {
		$options[$key] = $value['title'];
	}

	$form['new'] = array(
		'#type' => 'fieldset',
		'#title' => 'Add a new logger',
		'#collapsible' => TRUE, 
		'#collapsed' => FALSE,

		'type' => array(
			'#type' => 'select',
			'#options' => $options,
			'#description' => '',
			'#empty_option' => 'Select new logger type',
			'#required' => TRUE
		),
		'description' => array(
			'#type' => 'textfield',
			'#title' => 'Enter new logger description here',
			'#required' => TRUE,
		),
		'add' => array(
			'#type' => 'submit',
			'#value' => 'Add',
			'#ajax' => array(
				'callback' => 'watchman_logger_admin_ajax_callback',
				'wrapper' => 'instances',
				'method' => 'replace',
				'effect' => 'fade',	
			),
		),
	);
	$form['instances'] = array(
		'#type' => 'vertical_tabs',
		'#prefix' => '<div id="instances">',
		'#suffix' => '</div>',
	);

	$instances = watchman_get_logger_instances();
	foreach ($instances as $li) {
		$key = $li->type . '_' . $li->iid;
		$form['instances'][$key] = watchman_logger_instance_to_form_helper($li);
	}

	return $form;
}


function watchman_logger_instance_to_form_helper($li) {
	$i = watchman_logger_get_form($li->type, $li->config);

	$i['#title'] = $li->description;
	$i['#type'] = 'fieldset';
	$i['#group'] = 'instances';
	$i['#collapsible'] = TRUE;
    $i['#collapsed'] = TRUE;
	$i['#weight'] = $li->iid;

	$i['save'] = array(
			'#type' => 'submit',
			'#value' => 'Save',
			'#ajax' => array(
				'callback' => 'watchman_logger_admin_ajax_callback',
				'wrapper' => 'instances',
				'method' => 'append',
				'effect' => 'none',	
			),
		);
	$i['delete'] = array(
			'#type' => 'submit',
			'#value' => 'Delete',
			'#ajax' => array(
				'callback' => 'watchman_logger_admin_ajax_callback',
				'wrapper' => 'instances',
				'method' => 'replace',
				'effect' => 'fade',	
			),
		);

	return $i;
}

/**+
 * Implements hook_form_submit().
 */
function watchman_logger_admin_ajax_callback($form, &$form_state) {
	switch($form_state['clicked_button']['#value']) 
	{
	case 'Delete':
		return $form_state['clicked_button'];
		break;
	case 'Save':
		return $form_state['clicked_button'];
		break;
	case 'Add':
		$type = $form_state['values']['type'];
		$description = $form_state['values']['description'];

		$config = serialize(array()); /// TODO pickup the rest of the form and serialize it 

		$added = watchman_add_logger($type, $description, $config);
		$form['instance'][$added['type'] . '_' . $added['iid']] = watchman_logger_instance_to_form_helper($added);

		return $form['instances'];
	}
}